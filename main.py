
from cloudinary import api 
import cloudinary
from cloudinary.utils import cloudinary_url
import os
from flask import Flask, render_template, request,jsonify
from google import logging

# Instantiates a client
client = google.cloud.logging.Client()

# Retrieves a Cloud Logging handler based on the environment
# you're running in and integrates the handler with the
# Python logging module. By default this captures all logs
# at INFO level and higher
client.get_default_handler()
client.setup_logging()

cloudinary.config(
  cloud_name = os.environ.get("CLOUD_NAME"), 
  api_key = os.environ.get("API_KEY"), 
  api_secret = os.environ.get("API_SECRET") 
)

def delete_Image(publicId):
    try:
        cloudinary_response = cloudinary.api.delete_resources(public_ids=f"test-delete-image/{publicId}")
        logging.debug("Cloudinary responses: ", cloudinary_response)
        return cloudinary_response
    except ValueError as ValErr:
        logging.error(f"Error: {ValErr}")
        return jsonify(dict(Status="Error", Message="Missing dependencies in the json body")), 400
    


def http(request):
    if request.method != 'POST':
        return jsonify(dict(Status="Error", Message="Missing dependencies in the json body")), 405
    try:
        request_json = request.get_json(silent=True)
        logging.debug(f"body: {request_json}")
        if request_json and "public_id" in request_json :
            return delete_Image(request_json["public_id"])
        else:
            logging.error("JSON is invalid, or missing a property")
            raise ValueError("JSON is invalid, or missing a property")
    except ValueError as ValErr:
        logging.error(f"Error: {ValErr}")
        return jsonify(dict(Status="Error", Message="Missing dependencies in the json body")), 400
    
